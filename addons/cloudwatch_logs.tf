variable "cloudwatch_logs_enabled" {
  default = false
}

variable "cloudwatch_log_retention" {
  default     = 90
  description = "The number of days to keep logs"
}

variable "cloudwatch_image_tag" {
  default = "v1.4-debian-cloudwatch"
}

resource "kubernetes_namespace" "fluentd" {
  count = var.cloudwatch_logs_enabled ? 1 : 0

  metadata {
    name = "fluentd"

    labels = {
      managed-by = "Terraform"
    }
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  count             = var.cloudwatch_logs_enabled ? 1 : 0
  name              = "${var.cluster_name}"
  retention_in_days = var.cloudwatch_log_retention

  tags = {
    cluster = var.cluster_name
    Owner   = var.owner
  }
}

# ======================================
# Fluentd

resource "helm_release" "fluentd-cloudwatch" {
  count      = var.cloudwatch_logs_enabled ? 1 : 0
  name       = "fluentd-cloudwatch"
  repository = "incubator"
  chart      = "fluentd-cloudwatch"
  namespace  = "fluentd"

  values = [
    templatefile("${path.module}/config/fluentd-cloudwatch.yaml", { CLUSTER_NAME = var.cluster_name }),
  ]

  set {
    name  = "awsRole"
    value = aws_iam_role.fluentd[0].name
  }

  set {
    name  = "awsRegion"
    value = data.aws_region.current.name
  }

  set {
    name = "tag"
    value = var.cloudwatch_image_tag
  }

  # Uses kube2iam for credentials
  depends_on = [
    helm_release.kube2iam,
    aws_iam_role.fluentd,
    aws_iam_role_policy.fluentd,
    kubernetes_namespace.fluentd,
    kubernetes_service_account.tiller,
    kubernetes_cluster_role_binding.tiller_clusterrolebinding,
    null_resource.repo_add_incubator,
  ]
}

resource "aws_iam_role" "fluentd" {
  count = var.cloudwatch_logs_enabled ? 1 : 0
  name  = "${var.cluster_name}-fluentd"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    },
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/nodes.${var.cluster_name}"
        },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "fluentd" {
  count = var.cloudwatch_logs_enabled ? 1 : 0
  name = "${var.cluster_name}-fluentd"
  role = aws_iam_role.fluentd[0].id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogGroups",
        "logs:DescribeLogStreams",
        "logs:CreateLogGroup"
      ],
      "Resource": [
        "*"
      ]
    }
  ]
}
EOF

}

