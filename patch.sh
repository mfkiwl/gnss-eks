#!/usr/bin/env bash

set -e
#set -o xtrace
# 
# Patch EKS nodes
# 

if [ -z $1 ] || [ -z $2 ]; then
    echo "Error: environment and/or environments path variables not set, I don't know which environment you want to create"
    exit 1
fi

function fail_on_workspace {
    echo "$1 does not exist, please ensure apply.sh has been run"
    exit 1
}

export ENVIRONMENT=$1
export ENVIRONMENTSPATH=$2

# Build worker nodes
pushd nodes
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg
terraform workspace select "nodes-green" || fail_on_workspace "nodes-green"
terraform workspace select "nodes-blue" || fail_on_workspace "nodes-blue"

blue_enabled=$(terraform output enabled)
active_node_group=$([[ "$blue_enabled" = "true" ]] && echo "blue" || echo "green")
new_node_group=$([[ "$active_node_group" = "blue" ]] && echo "green" || echo "blue")

# Create new nodes
terraform workspace select "nodes-${new_node_group}"

echo "selected workspace nodes-${new_node_group}"

# Discover desired count
desired_node_capacity=$(terraform output node_desired_counts | tr ',' '\n' | sort -r | head -1)
if ! [[ $desired_node_capacity =~ ^[0-9]+$ ]]; then
    desired_node_capacity=1
fi

# Disable cluster autoscaler, so we don't scale whilst patching
kubectl scale deployments/cluster-autoscaler-aws-cluster-autoscaler --replicas=0 -n kube-system || echo 'Warning: cluster autoscaler not found'

terraform plan -out newgroup.plan -input=false \
    -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars" \
    -var group_enabled=true \
    -var node_group_name="$new_node_group" \
    -var desired_nodes_per_az="$desired_node_capacity"
terraform apply -auto-approve newgroup.plan
rm newgroup.plan

# Drain current nodes
../drain_and_wait.sh "$active_node_group"

# Destroy old nodes
terraform workspace select "nodes-$active_node_group"
terraform plan -out oldgroup.plan -input=false \
    -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars" \
    -var group_enabled=false \
    -var node_group_name="$active_node_group"
terraform apply -auto-approve oldgroup.plan
rm oldgroup.plan

# Enable cluster autoscaler
kubectl scale deployments/cluster-autoscaler-aws-cluster-autoscaler --replicas=1 -n kube-system || echo 'Warning: cluster autoscaler not found'

popd
