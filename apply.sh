#!/usr/bin/env bash

set -e
#set -o xtrace
# 
# Patch EKS nodes
# 

if [ -z $1 ] || [ -z $2 ]; then
    echo "Error: environment and/or environments path variables not set, I don't know which environment you want to create"
    exit 1
fi

export ENVIRONMENT=$1
export ENVIRONMENTSPATH=$2

if [[ "$3" = "clean" ]]; then
    CLEAN="true"
fi

# build network and EKS masters
pushd infra
if [ ! -z "$CLEAN" ]; then
    rm -rf .terraform
fi
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg
terraform workspace new "infra" || terraform workspace select "infra"
terraform plan -out infra.plan -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
terraform apply -auto-approve infra.plan
rm infra.plan

# Configure local kubernetes config
init-kubeconfig.sh "$ENVIRONMENT"

# Set up aws-auth
popd

# build worker nodes
pushd nodes
if [ ! -z "$CLEAN" ]; then
    echo "Cleaning"
    rm -rf .terraform
fi
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg

terraform workspace new "nodes-blue" || terraform workspace select "nodes-blue"
terraform plan -out workersblue.plan -input=false \
    -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars" \
    -var group_enabled=true \
    -var node_group_name=blue
terraform apply -auto-approve workersblue.plan
rm workersblue.plan

terraform workspace new "nodes-green" || terraform workspace select "nodes-green"
terraform plan -out workersgreen.plan -input=false \
    -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars" \
    -var group_enabled=false \
    -var node_group_name=green
terraform apply -auto-approve workersgreen.plan
rm workersgreen.plan
popd

pushd addons
if [ ! -z "$CLEAN" ]; then
    rm -rf .terraform
fi
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg
terraform workspace new "addons" || terraform workspace select "addons"
terraform plan -out addons.plan -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
terraform apply -auto-approve addons.plan
rm addons.plan
popd
