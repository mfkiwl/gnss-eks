#!/usr/bin/env bash


set -e
#set -o xtrace
# 
# Patch EKS nodes
# 

if [ -z $1 ] || [ -z $2 ]; then
    echo "Error: environment and/or environments path variable is not set, I don't know which environment you want to destroy"
    exit 1
fi

export ENVIRONMENT=$1
export ENVIRONMENTSPATH=$2

if [[ "$3" = "clean" ]]; then
    CLEAN="true"
fi

# delete addons
pushd addons
if [ ! -z "$CLEAN" ]; then
    rm -rf .terraform
fi
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg
terraform workspace new "addons" || terraform workspace select "addons"
terraform destroy -auto-approve -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"

popd

# delete worker nodes
pushd nodes
if [ ! -z "$CLEAN" ]; then
    rm -rf .terraform
fi
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg
terraform workspace new "nodes-blue" || terraform workspace select "nodes-blue"
terraform destroy -auto-approve -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"

terraform workspace new "nodes-green" || terraform workspace select "nodes-green"
terraform destroy -auto-approve -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"

popd

# delete network and EKS masters
pushd infra
if [ ! -z "$CLEAN" ]; then
    echo "cleaning"
    rm -rf .terraform
fi
terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg
terraform workspace new "infra" || terraform workspace select "infra"
terraform destroy -auto-approve -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
popd
