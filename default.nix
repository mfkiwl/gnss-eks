{ pkgs ? import ./nix {}, sources ? import ./nix/sources.nix }:

rec {
  inherit (import sources.gnss-common {}) assumeRoleTerraformStateReader;
  initKubeconfig = (pkgs.callPackage ./nix/init-kubeconfig.nix { inherit assumeRoleTerraformStateReader; });
}
