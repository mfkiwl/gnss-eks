{
  stdenv, lib, awscli, bash, busybox, git, jq, makeWrapper, openssh, terraform_0_12,
  assumeRoleTerraformStateReader
}:

let
  runtimeDependencies = [assumeRoleTerraformStateReader awscli bash busybox git jq openssh terraform_0_12];

in

  stdenv.mkDerivation rec {
    name = "init-kubeconfig";
    src = ./..;

    nativeBuildInputs = [
      makeWrapper
    ];

    phases = "installPhase";

    installPhase = ''
      mkdir -p $out/{bin,dist}

      cp -r $src/* $out/dist/
      chmod +x $out/dist/${name}.sh

      wrapProgram $out/dist/${name}.sh \
        --set PATH /usr/bin:${lib.makeBinPath runtimeDependencies}

      ln -s $out/dist/${name}.sh $out/bin/${name}.sh
    '';
  }
