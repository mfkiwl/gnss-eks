{ stdenv, fetchurl }:

stdenv.mkDerivation rec {
  pname = "helm";
  version = "2.14.1";

  src = fetchurl {
    url = "https://get.helm.sh/helm-v${version}-linux-amd64.tar.gz";
    sha256 = "15jqgx5hcg8mv79mm9frbj9lzdlx1yafhk9z6kqmwhw4d1g78kw0";
  };

  phases = [ "installPhase" ];

  installPhase = ''
    mkdir -p $out/bin
    tar -xvzf $src
    cp linux-amd64/helm $out/bin/${pname}
    chmod +x $out/bin/${pname}
    mkdir -p $out/share/bash-completion/completions
    $out/bin/helm completion bash > $out/share/bash-completion/completions/helm
  '';
}
