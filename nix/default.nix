{ sources ? import ./sources.nix }:

import sources.nixpkgs {
  overlays = [
    (pkgs: _: {
      # TODO: try `inherit (import sources.niv { pkgs }) niv` to avoid
      # doubling up on nixpkgs checkouts
      inherit (import sources.niv {}) niv;
      inherit (import sources.gnss-common {}) assumeRoleTerraformStateReader;
      initKubeconfig = pkgs.callPackage (import ./init-kubeconfig.nix) {};
      helm = pkgs.callPackage (import ./helm) {};
    })
  ];
}
