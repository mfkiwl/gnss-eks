# Cluster config
owner = "gnss-informatics"

cluster_name = "gnss-eks-test"

cluster_version = "1.17"

admin_access_CIDRs = {
  "Everywhere" = "0.0.0.0/0"
}

# Worker instances
default_worker_instance_type = "t3.large"

spot_nodes_enabled = false

min_nodes_per_az = 1

desired_nodes_per_az = 1

max_nodes_per_az = 2

max_spot_price = "0.4"

# RDS
gnss_rds_cluster = "gnss-rds-test"

# Database config

db_instance_enabled = false

# Addons - Kubernetes logs to cloudwatch

cloudwatch_logs_enabled = true

cloudwatch_log_group = "gnss-eks-test"

cloudwatch_log_retention = 90

cf_certificate_create = false

external_dns_enabled = true

external_dns_role_arn = "arn:aws:iam::688660191997:role/gnss-data-test-gnss-eks-external-dns"

txt_owner_id = "gnss-eks-test"

users = [
  "arn:aws:iam::023072794443:root", # gnss-common-nonprod
  "arn:aws:iam::688660191997:root", # geodesy-operations
]

efs_enabled = true

domain_name = ""

region = "ap-southeast-2"

# https://bitbucket.org/geoscienceaustralia/gnss-eks/addon/pipelines/home#!/results/705
ami_image_id = "ami-0f6282d5904ecc09e"
