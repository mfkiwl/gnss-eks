{
  pkgs ? import ./nix {},
  installDevTools ? false
}:

let

  projectName = "gnss-eks";

  buildTools = with pkgs; [
    assumeRoleTerraformStateReader
    aws-iam-authenticator
    awscli
    cacert
    git
    helm
    initKubeconfig
    openssh
    packer
    python3Packages.credstash
    jq
    kubectl
    terraform_0_12
  ];

  devTools = with pkgs; [
    niv
  ];

  env = pkgs.buildEnv {
    name = projectName + "-env";
    paths = buildTools ++ (
      if installDevTools then devTools else []
    );
  };

in

  pkgs.mkShell {
    buildInputs = [
      env
    ];
    shellHook = ''
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"
    '';
  }
