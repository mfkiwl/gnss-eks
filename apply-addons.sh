#!/usr/bin/env bash

# Deploy/update addons

set -euo pipefail

export ENVIRONMENT=$1
export ENVIRONMENTSPATH=$2
shift 2

dryRun=false

while [ $# -gt 0 ]; do
    case $1 in
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        * )
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

pushd addons

terraform init -backend-config "$ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg"
terraform workspace select "addons" || terraform workspace new "addons"

if [[ $dryRun = "false" ]]; then
    terraform plan -out addons.plan -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
    terraform apply -auto-approve addons.plan
    rm addons.plan
else
    terraform plan -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
fi
