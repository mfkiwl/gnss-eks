data "aws_region" "current" {}

data "external" "thumbprint" {
  program = ["${path.module}/oidc-provider-thumbprint.sh", data.aws_region.current.name]
}

resource "aws_iam_openid_connect_provider" "eks" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.external.thumbprint.result.thumbprint]
  url             = aws_eks_cluster.eks.identity.0.oidc.0.issuer
}
