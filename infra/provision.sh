#!/usr/bin/env bash

set -xeuo pipefail

sudo yum update kernel -y
sudo yum update -y

sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo yum install -y --nogpgcheck https://s3-ap-southeast-2.amazonaws.com/golden-ami-stack-test-goldenamiconfigbucket-2m56fudqmxlj/NessusAgent.rpm

curl https://inspector-agent.amazonaws.com/linux/latest/install | sudo bash
