#!/usr/bin/env bash
set -euo pipefail

current_nodegroup="$1"

kubectl taint nodes --all=true key:NoSchedule- || echo "Removed Taints"

# taint the existing nodegroup so no new pods will schedule
kubectl taint nodes -l nodegroup="$current_nodegroup" key=value:NoSchedule --overwrite=true

# ensure we have 2 dns pods running
kubectl scale deployments/coredns --replicas=2 -n kube-system

# Safely drain each node in the group
nodes_cmd="$(kubectl get nodes -l nodegroup="$current_nodegroup" -o jsonpath='{range .items[*]}{.metadata.name}{"\n"}{end}')"
nodes=()
while IFS='' read -r line; do nodes+=("$line"); done < <(echo "$nodes_cmd")
for i in "${nodes[@]}"
do
    echo "Draining node: $i"
    kubectl drain "$i" --ignore-daemonsets --delete-local-data || echo "Node $i no longer exists"

    # Wait max 15 mins per nodes for deployments to be healthy
    max_wait=900
    ready=false
    while [[ $max_wait -gt 0 ]] && [ $ready == false ]; do
        # Wait first to ensure the drain has started
        sleep 10
        max_wait=$(("$max_wait" - 10))
        echo "Waited 10 seconds. Still waiting max. $max_wait"

        # Check if deployments are healthy

        ready=true 

        deployments=$(kubectl get deployments --all-namespaces -o json)
        count=$(echo "$deployments" | jq -r '.items | length')
        for (( j=0; j<count; j++ )); do
            deployment_json=$(echo "$deployments" | jq -r ".items[$j]")

            # taken from: https://gist.github.com/gg7/3d632e77476235c020d9c434908accb1
            replicas_spec=$(echo "$deployment_json" | jq -r '.spec.replicas')
            replicas_status_present=$(echo "$deployment_json" | jq -r ".status.replicas")
            replicas_status_updated=$(echo "$deployment_json" | jq -r ".status.updatedReplicas // 0")
            replicas_status_unavail=$(echo "$deployment_json" | jq -r ".status.unavailableReplicas // 0")
            replicas_status_avail=$(echo "$deployment_json" | jq -r ".status.availableReplicas // 0")

            if [[ $replicas_status_updated -lt $replicas_spec ]] || \
                [[ $replicas_status_avail -lt $replicas_spec ]] || \
                [[ $replicas_status_present -lt $replicas_spec ]] || \
                [[ $replicas_status_unavail -gt 0 ]]; then
                printf "Deployment %s not ready, desired: %d; present: %d; available: %d; updated: %d; unavailable: %d\n" \
                    "$(echo "$deployment_json" | jq -r '.metadata.name')" \
                    "$replicas_spec" \
                    "$replicas_status_present" \
                    "$replicas_status_avail" \
                    "$replicas_status_updated" \
                    "$replicas_status_unavail"

                ready=false
            fi
        done
        echo "Deployments ready: $ready"
    done
done

# drain all pods from the nodes, continue even if we are using empytyDir 
kubectl drain -l nodegroup="$current_nodegroup" --ignore-daemonsets --delete-local-data
