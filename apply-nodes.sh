#!/usr/bin/env bash

# Create worker nodes. To update worker nodes use patch.sh.

set -euo pipefail

export ENVIRONMENT=$1
export ENVIRONMENTSPATH=$2

cd nodes

terraform init -backend-config $ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg

terraform workspace new "nodes-blue" || terraform workspace select "nodes-blue"
terraform plan -out workersblue.plan -input=false \
    -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars" \
    -var group_enabled=true \
    -var node_group_name=blue
terraform apply -auto-approve workersblue.plan
rm workersblue.plan

terraform workspace new "nodes-green" || terraform workspace select "nodes-green"
terraform plan -out workersgreen.plan -input=false \
    -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars" \
    -var group_enabled=false \
    -var node_group_name=green
terraform apply -auto-approve workersgreen.plan
rm workersgreen.plan
