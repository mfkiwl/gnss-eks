#!/usr/bin/env bash

# Build network and EKS masters

set -euo pipefail

export ENVIRONMENT=$1
export ENVIRONMENTSPATH=$2
shift 2

dryRun=false

while [ $# -gt 0 ]; do
    case $1 in
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        * )
            echo "Unknown option: $1"
            exit 1
            ;;
    esac
done

pushd infra

terraform init -backend-config "$ENVIRONMENTSPATH/$ENVIRONMENT/backend.cfg"
terraform workspace select "infra" || terraform workspace new "infra"

if [[ $dryRun = "false" ]]; then
    terraform plan -out infra.plan -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
    terraform apply -auto-approve infra.plan
    rm infra.plan
else
    terraform plan -input=false -var-file="$ENVIRONMENTSPATH/$ENVIRONMENT/terraform.tfvars"
fi
