# Cluster config
owner = "example-owner"

cluster_name = "dev-eks-example"

cluster_version = "1.17"

admin_access_CIDRs = {
  "Everywhere" = "0.0.0.0/0"
}

# Worker instances
default_worker_instance_type = "t3.medium"

spot_nodes_enabled = false

min_nodes_per_az = 1

desired_nodes_per_az = 1

max_nodes_per_az = 2

max_spot_price = "0.4"

# Database config

# Addons - Kubernetes logs to cloudwatch

cloudwatch_logs_enabled = false

cloudwatch_log_group = "example"

cloudwatch_log_retention = 90

alb_ingress_enabled = true
